# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from character import character
from being import agility, zen
from spells import *
from items import *
from game import game

class Party(object):
  def __init__(self, chars):
    self.chars = chars
    self.claim_chars()

  def is_dead(self):
    for char in self.chars:
      if char.is_alive():
        return False
    return True

  def living_chars(self):
    return [char for char in self.chars if char.is_alive()]

  def claim_chars(self):
    for char in self.chars:
      char.party = self

  def find_max(self, ability):
    max = -1
    whose = None
    for char in self.chars:
      if char.stats[ability] > max:
        max = char.stats[ability]
        whose = char

    return (max, whose)

  def max_zen(self):
    return self.find_max(zen)[1]

  def zen_bleed(self):
    max_zen = self.find_max(zen)[0]
    return max(int(max_zen * .9), max_zen - 5)

  def agility_bleed(self):
    max_agility = self.find_max(agility)[0]
    return max(int(max_agility * .9), max_agility - 5)

class HeroParty(Party):
  def __init__(self):
    self.gold = 1000
    self.items = []
    self.climb_level = 0
    chars = [character(name="FM", level=1, character_number=0, pronoun="he")]
    Party.__init__(self, chars)

  def got_item(self, item):
    for inv in self.items:
      if inv == item:
        inv.count += item.count
        return
    self.items.append(item)

  def lost_item(self, item, num):
    self.items[item].count -= num
    if self.items[item].count <= 0:
      del self.items[item]

  def get_items(self):
    return self.items

  def get_spells(self):
    spells = []
    if healing_stone in self.items:
      spells.append(heal)
    return spells
