# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

(attack, special_attack, special, attack_spell, defensive_spell, run, other, wasted_turn) = range(8)
event_types = ("attack", "special_attack", "special", "attack_spell", "defensive_spell", "run", "other", "wasted_turn")
