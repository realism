# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from mod_import import do_mod_import
do_mod_import("monsters", globals())

if id(default_monsters) != id(monsters):
  monsters = default_monsters + monsters

from random import choice as randchoice

class monster_db(object):
  def __init__(self):
    self.level_db = {}
    self.level_element_db = {}
    self.all_monsters = monsters

  def create_random_monster(self, level, element=None):
    # We could use self.random_monster(...)(...), but this is cleaner.
    return self.random_monster(level, element).create(level, element)

  def random_monster(self, level, element=None):
    return randchoice(self.get_monsters(level, element))

  def get_monsters(self, level, element=None):
    if element == None or element == Random:
      if level not in self.level_db.keys():
        self.level_db[level] = \
          [
             monster 
             for monster in self.all_monsters 
             if monster.can_be_level(level)
          ]
      return self.level_db[level]
    else:
      if (level, element) not in self.level_element_db.keys():
        self.level_element_db[level] = \
          [
             monster 
             for monster in self.all_monsters 
             if monster.can_be_level(level)
             if monster.can_be_element(element)
          ]
      return self.level_element_db[level]

monster_db = monster_db()
