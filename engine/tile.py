# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# This file contains several imports inside functions, to avoid poluting the
# namespace, which is imported by tiles.py.

from game import game
from engine.game_map import MapException
from utils import all, to_bool

# Represents a game_map for use in tile.argument.
a_map = None

def get_map_specials(game_map):
  from game_map import special_keys
  for key in special_keys:
    yield game_map.map_key[key][0]

def default_action(my_tile):
  # Do nothing if the player didn't move.
  if my_tile.pos[0] < 1 or not my_tile.passable():
    return

  game_map = my_tile.game_map

  # Gain health and mana.
  game_map.hp_mp_ticks()

  # If there's any more "grace period" left after a battle, use some.
  if game_map.delay_left > 0:
    game_map.delay_left -= 1
    return

  # Otherwise, see if there's a random encounter.
  from random import randint
  from engine.battle import simple_battle
  (chance, delay, minlvl, maxlvl, mincrits, maxcrits, crittypes, tileset) = \
    get_map_specials(game_map)

  if randint(1,100) < int(chance):
    # Battle!
    # Add the grace period.
    game_map.delay_left = int(delay)
    # Determine the level and number of monsters.
    level = randint(int(minlvl),int(maxlvl))
    crits = randint(int(mincrits),int(maxcrits))
    # Start the battle.
    simple_battle(level, crits)

def safe_action(my_tile):
  # Do nothing if the player didn't move.
  if my_tile.pos[0] < 1 or not my_tile.passable():
    return

  # Just gain health and mana.
  my_tile.game_map.hp_mp_ticks()

def shop_action(my_tile):
  # Enter the shop.
  game.ui.map_mode(my_tile.shop)

def save_action(my_tile):
  #TODO
  pass

def no_action(my_tile):
  # No action, so we do nothing.  How zen of us.
  pass

def can_climb(my_tile, climb_difficulty):
  # TODO
  return False

class tile(object):
  type = "empty"
  save = ()
  floaters = ()

  def __init__(self, map_pos, *args):
    # Where this tile is.
    (self.game_map, self.pos) = map_pos
    (self.target_map, self.target_pos) = map_pos

  def argument(self, name, arg, argument_type):
    try:
      if type == str:
        return arg
      elif argument_type in (int, float):
        return argument_type(arg)
      elif argument_type == a_map:
        return game.maps[arg]
      elif argument_type == bool:
        return to_bool(arg) 
      else:
        tile_type = self.__class__.__name__
        arg_type_name = argument_type.__name__
        raise MapException("Unknown argument type %s used by tile %s." 
                           %                (arg_type_name,    tile_type))

    except (ValueError, KeyError):
      tile_type = self.__class__.__name__

      argument_type_names = {int: "an integer", float: "a number", 
                             a_map: "a map"}
      argtype = argument_type_names[argument_type]

      raise MapException(
        "Bad %s passed to tile %s in map %s: Must be %s, but got %s."\
        % ( name,       tile_type, self.game_map.name, argtype, arg)
                        )

  def go_to(self, teleported = False):
    return self.move_ok(teleported)

  def target_tile(self):
    return self.target_map.tile_at(self.target_pos)

  def move_ok(self, teleported = False):
    if teleported:
      # If we teleported, shove floaters north.
      new_pos = (self.pos[0] - 1, self.pos[1])
    else:
      # Calculate where the floaters would be pushed to.
      new_pos = map(lambda a,b: 2*a-b, self.pos, self.game_map.pos)

    # See if they can all be pushed there.
    floaters_ok = all(floater.can_push_to(new_pos) for floater in self.floaters)

    # We can't move there unless we can push or overlap all the floaters.
    if not floaters_ok:
      return False

    # Remove any floaters that were pushed.
    self.floaters = filter(lambda f: f.pos == self.pos, self.floaters)

    # If we haven't teleported to this tile, try to use this tile's teleport.
    if not teleported:
      target_tile = self.target_tile()
      if target_tile != self:
        # If we go to the tile, we suppress the other actions of this tile.
        if target_tile.go_to(teleported = True):
          return None
        else:
          return True

    # Update the party's position.
    self.game_map.pos = self.pos
    if teleported:
      game.ui.map_mode(self.game_map)

    return True

  def hp_mp_ticks(self):
    self.game_map.hp_mp_ticks()

  def maybe_fight(self):
    game_map = self.game_map

    # If there's any more "grace period" left after a battle, use some.
    if game_map.delay_left > 0:
      game_map.delay_left -= 1
      return
  
    # Otherwise, see if there's a random encounter.
    from random import randint
    from engine.battle import simple_battle
    (chance, delay, minlvl, maxlvl, mincrits, maxcrits, crittypes, tileset) = \
      get_map_specials(game_map)
  
    if randint(1,100) < int(chance):
      # Battle!
      # Add the grace period.
      game_map.delay_left = int(delay)
      # Determine the level and number of monsters.
      level = randint(int(minlvl),int(maxlvl))
      crits = randint(int(mincrits),int(maxcrits))
      # Start the battle.
      simple_battle(level, crits)
