# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from being import being, will, zen
from random import randint, random
from elements import *
from battle_event import BattleEvent
from event_types import attack_spell, defensive_spell, wasted_turn

def spell_hits(caster, target, friendly, battle):
  if friendly:
    hit_chance = being.opposed_factor(caster.stats[will], battle.enemy_party(caster.party).zen_bleed() + 3)
  else:
    hit_chance = being.opposed_factor(caster.stats[will], target.stats[zen] + 3, target.party.zen_bleed() + 3)

  rand = random()
  #print "hit_chance %f, rand %f" % (hit_chance, rand)
  if rand >= hit_chance and not friendly:
    target.used_ability(caster,zen)
    return False
  elif rand >= hit_chance:
    battle.enemy_party(caster.party).max_zen().used_ability(caster,zen)
    return False

  #print "Hit."
  return True

def spell_damage(caster, target, battle, element, defensive, friendly, multiplier, cap):
  mean_damage = being.mean_damage(caster.stats[will]) * multiplier
  min_damage = max(1, int(mean_damage / 2))
  max_damage = max(1, int(mean_damage * 1.5))

  if friendly:
    zen_factor = being.opposed_factor(
                   caster.stats[will],
                   battle.enemy_party(caster.party).zen_bleed()
                 )
  else:
    zen_factor = being.opposed_factor(
                   caster.stats[will],
                   target.stats[zen],
                   battle.enemy_party(caster.party).zen_bleed()
                 )

  # 1 <= raw_damage <= cap
  raw_damage = max(1, 
                 int(
                   randint(min_damage, max_damage) * zen_factor
                 )
               )
  if cap != None:
    raw_damage = min(cap, raw_damage)

  if defensive:
    # Minus sign is the conversion from "damaging spell" to "healing spell".
    # Which is what a plain defensive spell is.
    element_factor = - defensive_element_interaction(element, target.element)
  else:
    element_factor = element_interaction(element, target.element)

  damage = int(element_factor * raw_damage)
  target.hp -= damage

  if damage < 0 and target.hp > target.max_hp:
    damage += target.hp - target.max_hp
    target.hp = target.max_hp

  # Healing enemies and damaging allies grants no XP.
  if friendly:
    effective_damage = - damage
  else:
    effective_damage = damage

  success = max(0.,float(effective_damage)/max_damage)
  caster.used_ability(target,will,success=success,battle=battle,min_opposed=battle.enemy_party(caster.party).zen_bleed())

  if target.hp < 1:
    caster.got_xp(target.level())
  elif not friendly:
    target.used_ability(caster,zen)

  return damage

def plain_spell(spell, caster, target, battle, mp_cost, element=no_element, defensive=False, multiplier=1., cap=None):
  if defensive:
    event_type = defensive_spell
  else:
    event_type = attack_spell

  event = BattleEvent(battle, caster, target, spell, event_type, result=None)
  if not caster.pay_mp(mp_cost):
    event.type = wasted_turn
    event.target = []
    event.result = []
    return event

  if caster.friendly(target) and target.friendly(caster):
    friendly = True
  else:
    friendly = False

  if not spell_hits(caster, target, friendly, battle):
    event.result = "miss"
    return event

  damage = spell_damage(caster, target, battle, element, defensive, friendly, multiplier, cap)

  if defensive and damage == 0:
    caster.mana_up(mp_cost)

  event.result = damage
  return event
