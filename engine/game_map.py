# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

(north, south, east, west) = range(4)
directions = border_names = ("north", "south", "east", "west")
special_keys = ("encounter_frequency", "encounter_delay", "encounter_min_level", "encounter_max_level", "encounter_min_critters", "encounter_max_critters", "critter_types", "tileset")

class MapException(Exception):
  pass

from shop import make_shop
from random import randint
from battle import simple_battle
from game import game
maps = game.maps
import tiles

class game_map(object):
  def __init__(self, name, map_lines):
    self.name = name
    self.game_map = []
    self.tiles = []
    self.map_key = {}
    self.delay_left = 0
    (ascii_art, key) = range(2)
    phase = ascii_art

    line_num = 0
    for line in map_lines:
      line_num += 1
      line = line.rstrip()
      if len(line) == 0 or line[0] == "'":
        #Comment or blank line.  Ignore it.
        pass
      elif ":" not in line:
        if phase != ascii_art:
          raise MapException("Error in processing map %s: non-key line %d found after start of key." % (name, line_num))
        self.game_map.append(line)
      else:
        phase = key
        (key, value_unsplit) = line.split(':')
        value = value_unsplit.split()
        self.map_key[key] = value

  def is_tile(self, key):
    return hasattr(tiles, key)

  def get_tile_class(self, key):
    return getattr(tiles, key)

  def is_game_map(self, key):
    return key in maps.keys()

  #def get_game_map(self, key):
  #  return maps[key]

  def make_tile(self, char, pos):
    tile_key = self.map_key[char][0]
    tile_args = self.map_key[char][1:]
    if self.is_tile(tile_key):
      tile_class = self.get_tile_class(tile_key)
      return tile_class((self, pos), *tile_args)
    elif self.is_game_map(tile_key):
      return tiles.map_tile((self, pos), tile_key, *tile_args)
    else:
      raise MapException("No tile or map named %s.  (Used by symbol %s in map %s)" % (tile_key, char, self.name))

  def validate(self):
    # Look for bad characters, collect all starting positions, and initialize
    # tiles.
    starts = []
    max_row_len = 0
    for row_num in range(len(self.game_map)):
      row = self.game_map[row_num]
      row_len = len(row)
      max_row_length = max(row_len, max_row_len)
      self.tiles.append([None]*row_len)
      for char_num in range(row_len):
        char = row[char_num]
        if char not in self.map_key.keys():
          message = "Error in processing map %s: Character missing from key.  Line %d:\n" % (self.name, row_num + 1)
          message += row + "\n"
          message += (" " * char_num) + "^"
          raise MapException(message)
        if self.map_key[char][0] in tiles.start_tiles:
          starts.append((row_num, char_num))
        self.tiles[row_num][char_num] = self.make_tile(char, (row_num, char_num))

    self.borders = [None] * 4
    for border in range(4):
      key = "border_" + border_names[border]
      self.borders[border] = self.make_tile(key, (-1, border))

    # More than one start point is fatal, 0 is bad but non-fatal.
    # If there is one, that's the starting position until set differently.
    if len(starts) > 1:
      raise MapException("More than one start point in map %s." % self.name)
    elif len(starts) == 0:
      print "Warning, map %s has no start point." % self.name
      self.start = None
    else:
      self.start = starts[0]
    self.pos = self.start

    # Each value in the map key should be either a function or map.  If neither,
    # that's a fatal error.
    for key in self.map_key.keys():
      if key in special_keys:
        continue
      value = self.map_key[key][0]
      if not hasattr(tiles, value):
        if value not in maps.keys():
          raise MapException("No tile or map named %s.  (Used by symbol %s in map %s)" % (value, key, self.name))

  def go(self, direction):
    pos = self.pos
    if pos != None:
      newpos = [p for p in pos]
      if direction == north:
        newpos[0] -= 1
      elif direction == south:
        newpos[0] += 1
      elif direction == west:
        newpos[1] -= 1
      elif direction == east:
        newpos[1] += 1
      return self.go_tile_at(newpos)

  def go_tile_at(self, pos):
    tile = self.get_tile(pos)
    return tile.go_to()

  def border(self, pos):
    (row, char) = pos
    # Return the appropriate border if it's beyond the edge of the map.
    if row < 0:
      return north
    elif char < 0:
      return west
    elif row >= len(self.tiles):
      return south
    elif char >= len(self.tiles[row]):
      return east
    else:
      return None

  def get_tile(self, pos):
    # Substitute start position if needed.
    if pos == 'start' or pos == ('s', 's'):
      pos = self.start

    (row, char) = pos

    border = self.border(pos)
    if border == None:
      return self.tiles[row][char]
    else:
      return self.borders[border]
  tile_at = get_tile

  def is_shop(self):
    return hasattr(self.get_tile(self.pos), "shop")

  def enter_shop(self):
    if not self.is_shop():
      return False

    # Get the shop.
    shop = self.get_tile(self.pos).shop
    
    # Show the shop.
    game.ui.shop_mode(shop)

  def hp_mp_ticks(self):
    party = game.party
    for i in range(len(party.chars)):
      game.ui.hp_tick(i, party.chars[i].hp_tick())
      game.ui.mp_tick(i, party.chars[i].mp_tick())

  def get_window(self):
    '''Returns a grid of tiles, centered on the player's location.'''
    (row, col) = self.pos
    if "window_size" in self.map_key.keys():
      window_size = int(self.map_key["window_size"])
    else:
      window_size = 2
    rowrange = range(row - window_size, row + window_size + 1)
    colrange = range(col - window_size, col + window_size + 1)

    window = tuple(tuple(self.get_tile((r, c)) for c in colrange) for r in rowrange)
    return window

def make_make_map(maps_file):
  from os.path import dirname, abspath, join
  directory = dirname(abspath(maps_file))
  def make_map(map_name):
    map_file = open(join(directory, map_name + ".map"))
    the_map = game_map(map_name, map_file.readlines())
    maps[map_name] = the_map
    return the_map
  return make_map
