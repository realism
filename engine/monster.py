# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from being import *
from new import instancemethod
from random import choice as randchoice
from game import game
party = game.party

(extremely_low, very_low, low, somewhat_low, slightly_low, normal, slightly_high, somewhat_high, high, very_high, extremely_high) = range(-5,6)
Random = -1

class monster(being):
  def __init__(self, being_args, name, ai_notify_func, ai_action_func):
    super(monster, self).__init__(*being_args)
    self.name = name
    self.something_happened = instancemethod(ai_notify_func, self, monster)
    self.do_something = instancemethod(ai_action_func, self, monster)

def default_ai_notify_func(self, event):
  pass

def default_ai_action_func(self, battle):
  target = randint(0, len(party.chars)-1)
  return self.attack(party.chars[target], battle)

def add_article(name):
  if name[0] in "aeiouAEIOU":
    return "an %s" % name
  else:
    return "a %s" % name

def default_name_func(name, element):
  if name == None and element == no_element:
    return None
  elif name == None:
    return add_article("%s elemental" % elements[element])
  return add_article("%s %s" % (element_descriptors[element], name))

def default_stat_func(level, element):
  hp = 3 * being.mean_damage(level)
  # +3 forces it to always round up.
  mp = (being.mean_damage(level) + 3)/4
  # No hacking stat.
  stat_change = min(1, level/5)
  stats = [randint(level-stat_change, level+stat_change) for stat in range(hacking)]
  return (hp, mp, stats)

def make_stat_adjusts(strength = normal, accuracy = normal, toughness = normal, agility = normal, will = normal, zen = normal, hacking = None):
  if hacking == None:
    return (strength, accuracy, toughness, agility, will, zen)
  else:
    return (strength, accuracy, toughness, agility, will, zen, hacking)

def only_elements(*elements):
  '''Yes, this is silly.  It's easier to read this way.'''
  return elements

def not_elements(*elements):
  ok_elements = range(element_count)
  for element in elements:
    ok_elements.remove(element)
  return ok_elements

def adjusted_stat_func(stat_adjusts, hp_multiplier = 1., mp_multiplier = 1.):
  def stat_func(level, element):
    hp = max(1, int(hp_multiplier * 3 * being.mean_damage(level)))
    mp = max(0, int(mp_multiplier * (being.mean_damage(level) + 3)/4))
    stat_change = min(1, level / 5)
    stats = []
    for stat in range(hacking):
      base_stat = randint(level-stat_change, level+stat_change)
      adjust = stat_adjusts[stat]
      stats.append(base_stat + adjust)
    return (hp, mp, stats)
  return stat_func

def custom_stat_func(hp, mp, stats):
  return lambda level, element: (hp, mp, stats)

# TODO: Part of me keeps screaming "NO!  BAD!  THIS IS THE WRONG WAY TO THIS!", so I suppose it should be fixed.
class monster_type(object):
  def __init__(self, name = None, min_level = 0, max_level = -1, default_element = no_element, ok_elements = range(element_count), ai_notify_func = default_ai_notify_func, ai_action_func = default_ai_action_func, name_func = default_name_func, stat_func = default_stat_func):
    self.name = name
    self.name_func = name_func
    self.min_level = min_level
    self.max_level = max_level
    self.default_element = default_element
    self.ok_elements = ok_elements
    self.ai_notify_func = ai_notify_func
    self.ai_action_func = ai_action_func
    self.stat_func = stat_func

  def __call__(self, *args, **kwargs):
    return self.create(*args, **kwargs)

  def can_be_level(self, level):
    if self.max_level == -1:
      return level >= self.min_level
    return level >= self.min_level and level <= self.max_level

  def can_be_element(self, element):
    return element in self.ok_elements

  def create(self, level, element = None):
    assert self.can_be_level(level), "Level for %s must be between %d and %d, got %d." % (self.name, self.min_level, self.max_level, level)
    if element == None:
      element = self.default_element
    if element == Random:
      element = randchoice(self.ok_elements)
    assert self.can_be_element(element), "Invalid element for %s: %s" % (self.name, elements[element])
    
    (hp, mp, stats) = self.stat_func(level, element)
    being_args = (stats, hp, mp, "%d%s%d" % (element, self.name, level/20), element)
    name = self.name_func(self.name, element)
    return monster(being_args, name, self.ai_notify_func, self.ai_action_func)
