# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from event_types import *

class BattleEvent(object):
  def __init__(self, battle, source, target, action, type, result):
    self.battle = battle
    self.source = source
    self.target = target
    self.action = action
    self.type = type
    self.result = result

  def isMultiTarget(self):
    target = self.target
    if hasattr(target, 'chars'):
      return True
    return type(target) == tuple or type(target) == list

  def getTargets(self):
    target = self.target
    if hasattr(target, 'chars'):
      return list(target.chars)
    if type(target) == tuple or type(target) == list:
      return list(target)
    return [target,]

  def isTarget(self, being):
    return being in self.getTargets()

  def getResults(self):
    result = self.result
    if type(result) == tuple or type(result) == list:
      return list(result)
    return [result]

  def isOffensive(self):
    return not self.source.friendly(self.getTargets()[0])

  def resultFor(self, being):
    if not self.isTarget(being):
      return 0
    return self.getResults()[self.getTargets().index(being)]

  def hurt(self, being):
    result = self.resultFor(being)
    return type(result) == int and result > 0

  def healed(self, being):
    return type(result) == int and result < 0
