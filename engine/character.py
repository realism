# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from being import *
class character(being):
  def __init__(self, name, level, character_number, pronoun):
    dummy_max_hp = 1
    stats = [level] * (hacking + 1)
    self.stats_xp = [0] * (hacking + 1)
    being.__init__(self, stats, dummy_max_hp, type="character")
    self.xp = 0
    self.character_number = character_number
    #self.stats = [4,5,4,2,5,1,1]
    #self.stats_xp = [.19,.03,.66,.1,0,0,0]
    self.redo_max_hp_mp()
    self.hp = self.max_hp
    self.mp = self.max_mp
    self.abilities_used = {}
    self.battles = {}
    self.name = name
    self.pronoun = pronoun
    if pronoun == "he":
      self.pronoun_object = "him"
    elif pronoun == "she":
      self.pronoun_object = "her"
    else:
      self.pronoun_object = pronoun

  def got_xp(self, amount):
    '''Leaving general xp dormant for the moment.  Maybe use it as a "purchase ability" pool?'''
    self.xp += amount

  def ensure_abilities_used(self, ability, target):
    if ability not in self.abilities_used.keys():
      self.abilities_used[ability] = {target: 0}
    elif target not in self.abilities_used[ability].keys():
      self.abilities_used[ability][target] = 0

  def ensure_battles(self, critter):
    if critter not in self.battles.keys():
      self.battles[critter] = 0

  def used_ability(self,target,ability,success=1.,min_opposed=0,battle=None):
    '''Adds appropriate experience in using an ability.
       Success should be between 0 (complete failure) and 1 (best possible result).'''
    if success <= 0:
      # If there was no success, we have nothing to do.
      return
    elif success >= 1:
      success = 1.

    if target.friendly(self) and self.friendly(target) and ability != will:
      # No XP for attacking allies.
      return

    if ability not in self.abilities_used.keys():
      self.abilities_used[ability] = {target: 0}
    elif target not in self.abilities_used[ability].keys():
      self.abilities_used[ability][target] = 0
    if target.type not in self.battles.keys():
      self.battles[target.type] = 0

    if ability == will and target.friendly(self) and self.friendly(target):
      assert battle != None, "Battle must be provided to used_ability by friendly spells."
      friendly_spell = True
      self.ensure_abilities_used(ability, 'friendly')
      diminish_ability = character.diminish_ability(self.abilities_used[ability]['friendly'])
      self.abilities_used[ability]['friendly'] += success
      #For friendly spells, the opposing critter is the enemy with the highest Zen.
      self.ensure_battles(battle.enemy_party(self.party).max_zen().type)
      diminish_critter = character.diminish_critter(self.battles[battle.enemy_party(self.party).max_zen().type],self.stats[ability])
    else:
      friendly_spell = False
      self.ensure_abilities_used(ability, target)
      diminish_ability = character.diminish_ability(self.abilities_used[ability][target])
      self.abilities_used[ability][target] += success
      self.ensure_battles(target.type)
      diminish_critter = character.diminish_critter(self.battles[target.type],self.stats[ability])

    diminishing_returns = diminish_ability * diminish_critter

    assert diminishing_returns >= 0, "Diminishing returns value below 0.  How'd that happen?  Value: %d" % diminishing_returns
    assert diminishing_returns <= 1, "Diminishing returns value above 1.  How'd that happen?  Value: %d" % diminishing_returns

    if friendly_spell:
      opposed_stat = min_opposed
    else:
      opposed_stat = max(min_opposed, target.stats[being.opposed(ability)])

    difficulty = character.difficulty(self.stats[ability], opposed_stat)

    if difficulty <= 0:
      difficulty = 0.

    ability_xp_gain = difficulty * diminishing_returns * success / self.uses_to_level(ability)
    self.stats_xp[ability] += ability_xp_gain

    if self.stats_xp[ability] >= 1.0:
      old_level = self.level()
      self.stats[ability] += 1
      self.stats_xp[ability] = 0.
      game.ui.gained_ability_level(self.character_number, ability)
      self.redo_max_hp_mp()
      if self.level() > old_level:
        game.ui.gained_level(self.character_number)

  def uses_to_level(self, ability):
    battles_to_level = self.stats[ability] + 3
    uses_per_battle = 1.75  # 3, diminished 50% each use
    return float(battles_to_level * uses_per_battle)

  def difficulty(mine, theirs):
    diff = mine - theirs
    if diff == 0:
      return 1
    elif diff < 0:
      return 1 - (.1 * diff)
    elif diff == 1:
      return .9
    elif diff == 2:
      return .7
    elif diff == 3:
      return .4
    elif diff == 4:
      return .1
    else:
      return 0
  difficulty = staticmethod(difficulty)

  def diminish_ability(previous_uses):
    return .5 ** previous_uses
  diminish_ability = staticmethod(diminish_ability)

  def diminish_critter(previous_battles, my_level):
    '''Calculates the diminishing factor for fighting a critter of the same type repeatedly.'''
    battles_to_level = my_level + 3
    # Over a large set of battles, the sum of the diminishing factors
    # converges to 2 * battles_to_level.
    # This is thrown off somewhat when previous_battles doesn't increase by 1
    # each time, as is often the case.
    diminishing_factor = 1 - (1. / (2. * battles_to_level))

    return diminishing_factor ** previous_battles
  diminish_critter = staticmethod(diminish_critter)

  def redo_max_hp_mp(self):
    '''Max HP := 5 times the average damage-per-hit of one average character of this level on another.'''
    hp_level = int(self.level()/2. + self.stats[toughness]/2.)
    self.max_hp = 5 * self.mean_damage(hp_level)
    mp_level = int(self.level()/2. + self.stats[will]/2.)
    # The +1 forces standard rounding.  Only works for /2.
    self.max_mp = (self.mean_damage(mp_level) + 1) / 2

  def pay_mp(self, amt):
    if self.mp >= amt:
      self.mp -= amt
      return True
    else:
      return False
