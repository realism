#!/usr/bin/env python
# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.


elements = ("no element", "fire", "ice", "lightning", "metal", "water", "air", "earth", "life", "death", "order", "chaos", "void", "ether", "rock", "paper", "scissors")
element_descriptors = ("plain", "firy", "frozen", "electrified", "metallic", "liquid", "lightweight", "heavy", "vigorous", "undead", "calm", "crazed", "jet black", "silvery", "rock", "paper", "scissors")
element_count=len(elements)
(no_element, fire, ice, lightning, metal, water, air, earth, life, death, order, chaos, void, ether, rock, paper, scissors) = range(element_count)

element_interaction_table = \
 (
  (1,)*element_count, # None is indifferent to all elements.
  (1,-1,2,1,.8,0,1,0,1,1.5,1.5,.5,.5,.5,1,1,1), # Fire attack
  (1,2,-1,1,.8,.8,1,1,1,1,.5,1.5,.5,.5,1,1,1), # Ice attack
  (1,1,.8,-1,0,2,1,.5,1,1,1.5,.5,.5,.5,1,1,1), # Lightning attack.
  (1,1,1,2,-1,0,.8,.5,1,1,1,1,.5,.5,1,1,1), # Metal attack
  (1,2,.8,0,2,-1,1,1.5,.5,1.2,1,1,.5,.5,1,1,1), # Water attack
  (1,.8,1,.5,1.5,1,-1,2,.5,1.5,1.5,.5,.5,.5,1,1,1), # Air attack
  (1,1.5,1,2,.5,.5,2,-1,1,1.5,.5,1.5,.5,.5,1,1,1), # Earth attack
  (1,1,1,.5,1,1,1,1,-1,2,.5,1.5,.5,.5,1,1,1), # Life attack
  (1,.5,1,1,1,1,1,1,2,-1,1.5,.5,.5,.5,1,1,1), # Death attack
  (1,1.5,.5,1.5,1,1,1,1.5,.5,.5,1.5,-1,2,.5,.5,1,1,1), # Order attack
  (1,.5,1.5,.5,1,1,1,.5,1.5,1.5,.5,2,-1,.5,.5,1,1,1), # Chaos attack
  (1,.5,.5,.5,.5,.5,.5,.5,.5,.5,.5,.5,-.5,2,1,1,1), # Void attack
  (1,.5,.5,.5,.5,.5,.5,.5,.5,.5,.5,.5,2,-.5,1,1,1), # Ether attack
  (1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,-1,2), # Rock attack
  (1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,0,-1), # Paper attack
  (1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,2,0) # Scissors attack
 )

def invert(multiplier):
  if multiplier == 0:
    return 0
  if multiplier <= -1:
    return 2
  if multiplier >= 2:
    return -1
  return 2 - multiplier

#Apply invert to each entry in element_interaction_table to invert it.
defensive_element_interaction_table = tuple(tuple(map(invert,line) for line in element_interaction_table))

def element_interaction(attacking, target):
  '''Returns a multiplication factor to apply when the attacking element is used against the target element.  1 is a no-op, >1 is extra damage, 0<x<1 is reduced damage, 0 is ignored, <0 is absorbed.'''
  return element_interaction_table[attacking][target]

def defensive_element_interaction(source, target):
  '''Returns a multiplication factor to apply when the source element is used to help the target element.  1 is a no-op, >1 is extra effect, 0<x<1 is reduced effect, 0 is ignored, <0 is inverted.  Don't Thaw Frosty, he will Melt!'''
  return defensive_element_interaction_table[source][target]

if __name__ == "__main__":
  for caster in range(element_count):
    for target in range(element_count):
      print "%-9s attacks %-9s : %1.1f" % (elements[caster], elements[target], element_interaction(caster, target))
      print "%-9s helps   %-9s : %1.1f" % (elements[caster], elements[target], defensive_element_interaction(caster, target))
