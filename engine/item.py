# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

class Item(object):
  def __init__(self, name, value, count):
    self.name = name
    self.value = value
    self.count = count

  def __eq__(self, other):
    if self.name != other.name:
      return False
    if self.value != other.value:
      return False
    return True

  def split(self, split_count):
    '''Splits this stack of items, returning a new stack with the requested size.'''
    self.count -= split_count
    return Item(self.name, self.value, split_count)

  def make(self, count):
    '''Creates a stack of count copies of this item.'''
    return Item(self.name, self.value, count)
