# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from random import randint, random
from elements import *
from battle_event import BattleEvent
from event_types import *
from game import game
attack_event = attack

(strength, accuracy, toughness, agility, will, zen, hacking) = range(7)
abilities = ("strength", "accuracy", "toughness", "agility", "will", "zen", "hacking")

Unique = None

class being(object):
  def __init__(self, stats, hp=1, mp=0, type=Unique, element=no_element):
    '''stats = [strength, accuracy, toughness, agility, will, zen, hacking]
       The hacking stat is optional.'''
    self.stats = stats
    self.max_hp = hp
    self.hp = hp
    self.max_mp = mp
    self.mp = mp
    if type != Unique:
      self.type = type
    else:
      self.type = id(self)
    self.element = element
    self.name = None
    self.pronoun = "it"
    self.pronoun_object = "it"

  def is_alive(self):
    return self.hp > 0

  def friendly(self, other):
    return self.party == other.party

  def opposed_factor(offensive, defensive, min_def = -5):
    diff = (max(min_def, defensive) - offensive) / 2.
    #print diff
    return .9 ** (diff * abs(diff))
  opposed_factor = staticmethod(opposed_factor)

  def opposed(ability):
    if ability == strength:
      return toughness
    if ability == accuracy:
      return agility
    if ability == toughness:
      return strength
    if ability == agility:
      return accuracy
    if ability == will:
      return zen
    if ability == zen:
      return will
    if ability == hacking:
      return hacking
  opposed = staticmethod(opposed)

  def attack(attacker, defender, battle):
    '''Calculates a standard attack.'''
    # attack should work here, but I'm using attack_event for clarity.
    event = BattleEvent(battle, attacker, defender, being.attack, attack_event, None)
    hit_chance = being.opposed_factor(attacker.stats[accuracy], defender.stats[agility] + 3, defender.party.agility_bleed() + 3)

    rand = random()
    #print "hit_chance %f, rand %f" % (hit_chance, rand)
    if rand >= hit_chance:
      defender.used_ability(attacker,agility)
      event.result = "miss"
      return event

    attacker.used_ability(defender,accuracy,min_opposed=defender.party.agility_bleed())

    mean_damage = being.mean_damage(attacker.stats[strength])
    min_damage = max(1, mean_damage / 2)
    max_damage = max(1, int(mean_damage * 1.5))

    damage = max(1, int(randint(min_damage, max_damage) * \
             being.opposed_factor(
               attacker.stats[strength], defender.stats[toughness]
             )))
    defender.hp -= damage

    attacker.used_ability(defender,strength,success=float(damage)/max_damage)

    if defender.hp < 1:
      attacker.got_xp(defender.level())
    else:
      defender.used_ability(attacker,toughness)

    event.result = damage
    return event

  def level(self):
    '''Being level is the average of their stats, rounded down.
       If they have the hacking stat, their lowest stat is dropped.'''
    if len(self.stats) > hacking:
      return (sum(self.stats) - min(self.stats)) / (len(self.stats) - 1)
    else:
      return sum(self.stats) / len(self.stats)
  
  def got_xp(self, xp):
    '''Adds XP, if this is a player character.'''
    pass
  
  def got_ability_xp(self, ability, xp):
    '''Adds ability XP, if this is a player character.'''
    pass

  def run(runner, enemy):
    runnum = max(1, 6 + runner.stats[agility] - enemy.stats[accuracy])
    
    if randint(1,10) > runnum:
      enemy.used_ability(runner, accuracy)
      return False

    if runnum < 10:
      runner.used_ability(enemy, agility)

    return True

  def health(self):
    if self.hp > (self.max_hp * .8):
      return "healthy"
    elif self.hp > (self.max_hp * .5):
      return "bruised"
    elif self.hp > (self.max_hp * .2):
      return "injured"
    elif self.hp > 0:
      return "near death"
    else:
      return "dead (AIEEEE!  ZOMBIE!)"

  def healed(self, amt):
    old_hp = self.hp
    self.hp = min(self.max_hp, old_hp + amt)
    return self.hp - old_hp

  def hp_tick(self):
    return self.healed(self.level())

  def mana_up(self, amt):
    old_mp = self.mp
    self.mp = min(self.max_mp, old_mp + amt)
    return self.mp - old_mp

  def mp_tick(self):
    return self.mana_up(self.level())

  def mean_damage(strength):
    if strength < 1:
      mean_damage = 1
    else:
      mean_damage = (strength ** 2) - (2 * strength) + 2
    #print "Strength %d, toughness %d: mean damage %d." % (strength, toughness, mean_damage)
    return mean_damage

  mean_damage = staticmethod(mean_damage)

  def used_ability(self,target,ability,success=1.,min_opposed=0,battle=None):
    '''Implemented in character, unused in being.'''
    pass

  def lots_happened(self, what_happened):
    '''Shorthand for sending a bunch of events at once.'''
    for event in what_happened:
      self.something_happened(event)

  def something_happened(self, event):
    '''Called whenever something happens in the battle.  Only monsters with AI care about this.'''
    # Debugging code.
    if False:
      print "%s got an event:" % self.name
      print "Source: %s" % event.source.name
      print "Targets:",
      for target in event.getTargets():
        print target.name,
      print
      print "Action: %s" %  event.action
      print "Type: %s" % event_types[event.type]
      print "Results:",
      for result in event.getResults():
        print result,
      print

  def do_something(self, battle):
    '''Tell a monster to take its turn.  If it has no AI, it attacks at random.'''
    target = randint(0, len(game.party.chars)-1)
    return self.attack(game.party.chars[target], battle)

def simple_critter(level):
  hp = 3 * being.mean_damage(level)
  # No hacking stat.
  stat_change = min(5, level/5)
  stats = [randint(level-stat_change, level+stat_change) for stat in range(hacking)]
  return being(stats, hp)
