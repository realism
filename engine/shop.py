# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from items import *
from game import game
party = game.party

class shop(object):
  def __init__(self, contents):
    self.contents = contents
    self.redo_stocked_contents()

  def redo_stocked_contents(self):
    self.stocked_contents = [item for item in self.contents if item.count > 0]

  def getContents(self):
    return self.stocked_contents

  def buy(self, stocked_item, num = 1):
    # Translate stocked_item back into the index for self.contents.
    item = 0
    for real_item in self.contents:
      if real_item == self.stocked_contents[stocked_item]:
        break
      item += 1

    price = self.contents[item].value
    total = num * price
    if num > self.contents[item].count:
      return "The store doesn't have that many.\n"
    elif party.gold < total:
      return "Not enough money.\n"
    party.gold -= total
    player_item = self.contents[item].split(num)
    party.got_item(player_item)
    if self.contents[item].count <= 0:
      self.redo_stocked_contents()

  def sell(self, item, num = 1):
    if party.items[item].count < num:
      return "You don't have that many.\n"
    shop_price = party.items[item].value
    price = shop_price / 2
    party.gold += num * price
    for shop_item in self.contents:
      if shop_item == party.items[item]:
        shop_item.count += num
        self.redo_stocked_contents()
        break
    party.lost_item(item, num)

def make_shop(level=1, weapons = False, armor = False, magic = False, other = False, random = True, contents = None):
  if random:
    if contents == None:
      contents = []
    if weapons:
      contents.append(sword.make(1))
    if armor:
      contents.append(shield.make(1))
    if magic:
      contents.append(fire_stone.make(1))
      contents.append(healing_stone.make(1))
    if other:
      contents.append(potion.make(1))

  return shop(contents)
