# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

from random import randint
from being import being, simple_critter, strength, toughness
from party import Party
from battle_event import BattleEvent
from event_types import run
from monsters import monster_db, Random
from game import game

# For some reason, map being defined elsewhere screws up the builtin.
map = __builtins__['map']

class battle(object):
  def __init__(self, level, count):
    self.party = Party([])
    self.party.chars = []
    for i in range(count):
      self.party.chars.append(monster_db.create_random_monster(level, element=Random))
      if self.party.chars[i].name == None:
        self.party.chars[i].name = "monster %d" % i
    self.party.claim_chars()
    self.size = len(self.party.chars)
    #print "Critter str:", party.chars[0].stats[strength]
    #print "Player tough:", game.party.chars[0].stats[toughness]

  def attack(self, char, monster):
    event = game.party.chars[char].attack(self.party.chars[monster], self)
    for critter in self.party.chars:
      critter.something_happened(event)
    return event

  def cast(self, caster, target, spell):
    event = spell(caster, target, self)
    for critter in self.party.chars:
      critter.something_happened(event)
    return event

  def group_defend(self):
    results = []
    for critter in self.party.chars:
      results.append( critter.do_something(self) )
    for critter in self.party.chars:
      critter.lots_happened(results)
    return results

  def defend(self, char, monster):
    return self.party.chars[monster].attack(game.party.chars[char])

  def run(self, char):
    monsters = [critter for critter in self.party.chars]
    event = BattleEvent(self, game.party.chars[char], monsters, being.run, run, [])
    for monster in event.getTargets():
      event.result.append(event.source.run(monster))
    for monster in event.getTargets():
      monster.something_happened(event)
    self.remove_critters(event)
    return event

  def over(self):
    for critter in range(len(self.party.chars)-1,-1,-1):
      if self.party.chars[critter].hp <= 0:
        self.dead(critter)
    if len(self.party.chars) == 0:
      game.ui.won_battle()
      for char in game.party.chars:
        char.abilities_used = {}
      return True
    return False

  def remove_critters(self, run_event):
    '''Removes any critters run away from.'''
    for (critter, removed) in map(None, self.party.chars, run_event.getResults()):
      if removed:
        self.party.chars.remove(critter)

  def enemy_party(self, party):
    if party != self.party:
      assert party == game.party, "Unknown party passed to battle.enemy_party.  %s is not %s (monster party) or %s (hero party)." % (party, self.party, game.party)
      return self.party
    else:
      return game.party

  def dead(self, critter):
    for char in game.party.chars:
      if self.party.chars[critter].type not in char.battles.keys():
        char.battles[self.party.chars[critter].type] = 0
      char.battles[self.party.chars[critter].type] += 1. / len(game.party.chars)
    del self.party.chars[critter]

def simple_battle(difficulty=1, count=1):
  game.ui.battle_mode(battle(difficulty, count))
