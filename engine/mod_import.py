# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# This file handles importing modules from a specific mod, or, if the module is
# not present, from the main game.
from args import mod

def do_mod_import(module, globals):
  # Always load and copy from the default module.
  default_module = __import__('main_game.%s' % module, globals, locals(), [''])

  # Yes, this is ugly.  Unfortunately, this is the only way to emulate 
  # from mods.<modname>.<module> import *.  Since this is the cleanest way to 
  # use monsters, items, etc., this is what we do.
  globals.update(default_module.__dict__)
  try:
    # If the mod module is available as well, copy from that too.
    module = __import__('mods.%s.%s' % (mod, module), globals, locals(), [''])
    globals.update(default_module.__dict__)
  except ImportError:
    pass
