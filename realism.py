#!/usr/bin/env python
# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

'''The main file of the Realism RPG engine.  Runs the main game, or whatever mod is specified.  This file is not meant to be imported, doing so may have strange effects.'''

# Realism separates the code into three main pieces: UI, engine, and game/mod.
# The ui used will be one of the modules in the ui subdirectory.  It handles all
# direct interaction with the user.
# The engine is stored in the engine subdirectory and consistes of all of the 
# generic code that doesn't deal directly with the game and its story, such as
# the combat mechanics.  It also has virtual modules that load data from the
# game and mod.
# Everything that can be changed by a mod is stored in the main_game
# subdirectory.  This includes monsters, spells, maps, and items.  Actual mods
# can be put in a mods subdirectory of Realism's main directory, or in the 
# user's home directory (as explained below).


# We allow mods to exist in the user's home directory, path:
# ~/.realism-rpg/mods/<modname>/
# On Windows XP, this translates to:
# <drive>:\Documents and Settings\<user>\.realism-rpg\mods\<modname>\
import sys
from os import makedirs
from os.path import join, expanduser, isdir
user_dir = join(expanduser('~'),'.realism-rpg')
if not isdir(user_dir):
  makedirs(user_dir)
sys.path.append(user_dir)

from game import game

# Initialize the party.
from engine.party import HeroParty
game.party = HeroParty()

# Initialize the UI.
from ui.zork_ui import ZorkUI
game.ui = ui = ZorkUI()

# Import the maps, register the game object with them, and validate them.
from engine.maps import maps
for game_map in maps.values():
  game_map.validate()

try:
  from engine.maps import start_map
except ImportError:
  if "start_map" not in maps.keys():
    print "No start map found.  Realism will exit."
    sys.exit()
  
  # maps["start"] guaranteed to exist.
  start_map = maps["start_map"]

# start_map guaranteed to exist

# Tell the UI to use start_map.
ui.map_mode(start_map)

# Start the UI.
ui.main_loop()
