# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# This file is dual-licensed under the LGPL.

'''This module holds a few generic utilities that could be of use in many programs.

to_bool(value): converts a string to a boolean semantically, as for a config file.
any, all: Implementations of these standard functions for older versions of Python.'''

def to_bool(value):
  '''Converts a string to a boolean semantically.  Usage:
     to_bool("tRuE") -> True
     to_bool("no") -> False
     to_bool("spam") -> raises ValueError

     Acceptable values (case insensitive):
     True: t, true, y, yes, on, enable, 1
     False: f, false, n, no, off, disable, 0'''
  false = ['f','false','n','no','off', 'disable', '0']
  true = ['t','true','y','yes','on', 'enable', '1']
  temp = str(value).strip().lower()
  if temp in false:
    return False
  elif temp in true:
    return True
  else:
    raise ValueError("Cannot parse boolean from input: %s" % value)

# Allows all and any to handle either one iterable object or a set of arguments.
def _arglist(*args):
  '''Returns an iterable, either the sole argument passed to it or the entire argument tuple.  Usage:
     _arglist(a_list) -> a_list
     _arglist(foo, bar, baz) -> (foo, bar, baz)
     _arglist("hiya") -> ("hiya",)'''
  from types import GeneratorType as generator
  if len(args) == 1 and type(args[0]) in (list, tuple, generator):
    return args[0]
  else:
    return args

def all(*args):
  '''Legacy implementaiton of Python's all function.  Returns true if all elements are true.  Usage:
     all(list_of_true_elements) -> True
     all(True, True, True) -> True
     all(True, False) -> False
     all() -> True'''
  AND = lambda a,b: a and b
  return reduce(AND, _arglist(*args), True)

def any(*args):
  '''Legacy implementation of Python's any function.  Returns true if any element is true.  Usage:
     any(list_with_a_true_element) -> True
     any(False, True, False) -> True
     any(False, False) -> False
     any() -> False'''
  OR = lambda a,b: a or b
  return reduce(OR, _arglist(*args), False)
