# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

'''Holds the text-based UI, ZorkUI.  This UI is pure text, and is therefore completely accessible to blind users.'''

from sys import stdin, stdout, exit
from ui import UI
from os import name as os_name
from engine.being import abilities
from engine.spells import *
from engine.game_map import north, south, east, west, directions
from game import game
party = game.party

# If it's available, use the readline-enhanced version of raw_input.
try:
  import readline
except ImportError:
  # Eventually, there should be a minimal wx GUI here that basically implements
  # history and navigation a la readline.  For now, we just fall back to the
  # basic raw_input.
  pass

def copy_docstr(func):
  '''Copies the docstring from the matching function on UI.'''
  func_name = func.func_name
  func.__doc__ = getattr(UI, func_name).__doc__

class ZorkUI(UI):
  '''The text-based UI.  Accessible to blind users.'''
  class_name = "Zork UI"

  def __init__(self):
    pass
  copy_docstr(__init__)

  def main_loop(self):
    input = 'look'
    while True:
      self.process_input(input)
      input = self.get_input()
  copy_docstr(main_loop)

  def get_input(self):
    '''Gets case-insensitive input from the user.'''
    return raw_input("> ").lower()

  def process_input(self, input):
    '''Processes input from the user.'''
    if not input:
      return
    if self.mode == 'map':
      game_map = self.game_map
      if input in ('l', 'look'):
        stdout.write("You are in a stony field.  ")
        open = []
        blocked = []
        window = self.game_map.get_window()
        my_col = my_row = len(window)/2
        if window[my_row - 1][my_col].type == "empty":
          open.append("north")
        else:
          blocked.append("north")
        if window[my_row + 1][my_col].type == "empty":
          open.append("south")
        else:
          blocked.append("south")
        if window[my_row][my_col + 1].type == "empty":
          open.append("east")
        else:
          blocked.append("east")
        if window[my_row][my_col - 1].type == "empty":
          open.append("west")
        else:
          blocked.append("west")
        if len(open) == 4:
          stdout.write("\n")
        elif len(open) == 3:
          stdout.write("A large rock blocks your path " + blocked[0] + ".\n")
        elif len(open) == 2:
          stdout.write("Large rocks block your path " + blocked[0] + " and " + blocked[1] + ".\n")
        elif len(open) == 1:
          stdout.write("Large rocks surround you.  There is a clear path " + open[0] + ".\n")
        else:
          stdout.write("Large rocks surround you, blocking all movement.\n")
        if game_map.is_shop():
          stdout.write("There is a shop here.\n")
        print game_map.name, game_map.pos
      elif input in ('n', 'north'):
        if game_map.go(north) != False:
          self.process_input('look')
        else:
          stdout.write("A large rock blocks your path.\n")
      elif input in ('s', 'south'):
        if game_map.go(south) != False:
          self.process_input('look')
        else:
          stdout.write("A large rock blocks your path.\n")
      elif input in ('e', 'east'):
        if game_map.go(east) != False:
          self.process_input('look')
        else:
          stdout.write("A large rock blocks your path.\n")
      elif input in ('w', 'west'):
        if game_map.go(west) != False:
          self.process_input('look')
        else:
          stdout.write("A large rock blocks your path.\n")
      elif input in ('enter', 'in', 'shop') or (input == 'i' and game_map.is_shop()):
        if game_map.isShop():
          game_map.enterShop()
          self.process_input('look')
        else:
          stdout.write("There's no shop here!\n")
      elif input in ('i', 'inv', 'inventory'):
        i = 0
        for item in party.get_items():
          print "%2d) %-15s %5s Gold (%s)" % (i, item.name, item.value, item.count)
          i += 1
        if i == 0:
          print "You are empty-handed."
        print "You have %d gold." % party.gold
      elif input in ('c', 'char', 'character'):
        for player in party.chars:
          print "Stats for %s:" % (player.name,)
          print " Level:", player.level()
          print " HP: %d/%d" % (player.hp, player.max_hp)
          print " MP: %d/%d" % (player.mp, player.max_mp)
          for ability in range(len(player.stats)):
            print " %-10s %d (%2d%%)" % \
              (abilities[ability].capitalize() + ":", 
               player.stats[ability], 
               player.stats_xp[ability] * 100)
          print
      elif input == 'xyzzy':
        self.map_mode(game.maps['beginner_woods'])
        self.process_input('look')
        import engine.items
        game.party.items.append(engine.items.healing_stone)
      elif input in ('q', 'quit', 'exit'):
        exit()
      else:
        stdout.write("I don't understand that.\n")
    elif self.mode == 'shop':
      split = input.split()
      if input in ('l', 'look'):
        i = 0
        print "Shop contents:"
        for item in self.shop.getContents():
          print "%2d) %-15s %5s Gold (%s)" % (i, item.name, item.value, item.count)
          i += 1
        if i == 0:
          print "The shop is empty."
        print "You have %d gold." % party.gold
      elif input in ('o', 'out', 'e', 'exit'):
        self.mode = 'map'
        self.process_input('look')
      elif split[0] in ('b', 'buy'):
        if len(split) == 1:
          stdout.write("Buy what?\n")
        elif len(split) > 3:
          stdout.write("I don't understand that.\n")
        else:
          try:
            item = int(split[1])
            if len(split) == 3:
              num = int(split[2])
            else:
              num = 1
            if item >= 0 and item < len(self.shop.getContents()):
              msg = self.shop.buy(item, num)
              if msg:
                stdout.write(msg)
              else:
                self.process_input('look')
            else:
              stdout.write("I don't see that item.\n")
          except ValueError:
            stdout.write("I don't understand that.\n")
      elif split[0] in ('s', 'sell'):
        if len(split) == 1:
          stdout.write("Sell what?\n")
        elif len(split) > 3:
          stdout.write("I don't understand that.\n")
        else:
          try:
            item = int(split[1])
            if len(split) == 3:
              num = int(split[2])
            else:
              num = 1
            if item >= 0 and item < len(party.get_items()):
              msg = self.shop.sell(item, num)
              if msg:
                stdout.write(msg)
              else:
                self.process_input('look')
            else:
              stdout.write("I don't see that item.\n")
          except ValueError:
            stdout.write("I don't understand that.\n")
      elif input in ('i', 'inv', 'inventory'):
        i = 0
        for item in party.get_items():
          print "%2d) %-15s %5s Gold (%s)" % (i, item.name, item.value, item.count)
          i += 1
        if i == 0:
          print "You are empty-handed."
        print "You have %d gold." % party.gold
      elif input in ('q', 'Q', 'quit', 'Quit'):
        exit()
      else:
        stdout.write("I don't understand that.\n")
    elif self.mode == "battle":
      if input in ('l', 'look'):
        for critter in self.battle.party.chars:
          print "%s looks %s." \
                  % (critter.name.capitalize(), critter.health())
      elif input in ('r', 'run', 'flee'):
        event = self.battle.run(0)
        critters = event.getTargets()
        results = event.getResults()
        for (critter, escaped) in map(None, critters, results):
          if escaped:
            print "You escape from %s!" % critter.name
          else:
            print "%s follows you." % critter.name.capitalize()
        if not self.battle.over():
          self.was_attacked(self.battle.group_defend())
      elif input in ('a', 'attack'):
        if not self.i_attacked(0, self.battle.attack(0, 0)):
          self.was_attacked(self.battle.group_defend())
      elif input in ('h', 'heal'):
        if heal in party.get_spells():
          me = party.chars[0]
          mana = me.mp
          event = self.battle.cast(me, me, heal)
          mana -= me.mp
          if event.target == []:
            print "You don't have enough mana!"
          else:
            healing = -event.result
            print "You spend %d mana to heal yourself for %d health." % (mana, healing)
            print "You now have %d/%d health and %d/%d mana." % \
                           (me.hp, me.max_hp, me.mp, me.max_mp)
            self.was_attacked(self.battle.group_defend())
        else:
          print "You need a healing stone to cast Heal."
      elif input in ('f', 'fire', 'fireball'):
        me = party.chars[0]
        target = self.battle.party.chars[0]
        mana = me.mp
        event = self.battle.cast(me, target, fireball)
        mana -= me.mp
        target = event.target
        damage = event.result

        if target == []:
          print "You don't have enough mana!"
        elif damage == "miss":
          print "You spend %d mana to cast a fireball at %s, but it is resisted." % (mana, target.name)
        elif target.hp > 0 and damage > 0:
          print "You spend %d mana to cast a fireball at %s, dealing %d damage." % (mana, target.name, damage)
        elif target.hp <= 0 and damage > 0:
          print "You spend %d mana to cast a fireball at %s, killing %s." % (mana, target.name, target.pronoun_object)
        elif damage == 0:
          print "You spend %d mana to cast a fireball at %s, with no effect." % (mana, target.name)
        else:
          print "You spend %d mana to cast a fireball at %s, healing %s." % (mana, target.name, target.pronoun_object)
        if target != [] and not self.battle.over():
          self.was_attacked(self.battle.group_defend())
      elif input in ('q', 'quit'):
        exit()
      else:
        stdout.write("I don't understand that.\n")
    else:
      stdout.write("Unsupported UI mode.\n")
      exit()

  def i_attacked(self, character, event):
    '''Prints messages for an attack by the (only) character.'''
    if event.target.hp <= 0:
      print "You killed %s!" % event.target.name
      if self.battle.over():
        return True
    elif event.result == "miss":
      print "You missed."
    else:
      print "You hit %s for %d damage." % (event.target.name, event.result)

  def was_attacked(self, events):
    '''Prints messages for attacks from enemies.'''
    for event in events:
      print event.source.name.capitalize(),
      if event.result == "miss":
        print "missed."
      else:
        print "hit %s for %d damage.  %s has %d health left." % \
         (event.target.name, event.result, event.target.pronoun.capitalize(), event.target.hp)
    if party.is_dead():
      print "You have been slain!"
      exit()

  def hp_tick(self, character, amount):
    if amount > 0:
      print "Your health increases by %d, to a total of %d." % (amount, party.chars[character].hp)
  copy_docstr(hp_tick)

  def mp_tick(self, character, amount):
    if amount > 0:
      print "You gain %d mana.  You have %d mana." % (amount, party.chars[character].mp)
  copy_docstr(mp_tick)

  def gained_ability_level(self, character, ability):
    print "Your %s improves!" % abilities[ability]
  copy_docstr(gained_ability_level)

  def gained_level(self, character):
    print "You gain a level!"
  copy_docstr(gained_level)

  def won_battle(self):
    self.map_mode()
    self.process_input("look")
  copy_docstr(won_battle)

  def map_mode(self, game_map = None):
    self.mode = 'map'
    if game_map:
      self.game_map = game_map
  copy_docstr(map_mode)

  def shop_mode(self, shop = None):
    self.mode = 'shop'
    if shop:
      self.shop = shop
  copy_docstr(shop_mode)

  def battle_mode(self, battle = None):
    self.mode = 'battle'
    if battle:
      self.battle = battle
    print "You are attacked by %d critters!" % len(battle.party.chars)
  copy_docstr(battle_mode)

  def conversation_mode(self, conversation = ""):
    print conversation
  copy_docstr(conversation_mode)
