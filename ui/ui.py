# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

'''Holds the generic user interface, UI.'''

class NotImplementedException(Exception): pass

class UI(object):
  '''The generic user interface.'''
  class_name = "Generic UI"

  def ni(self, function = None):
    '''Throws a "not implemented" exception.  If function name is provided, it is included in the exception.'''
    if function:
      raise NotImplementedException(function + " is not implemented by " + self.class_name)
    raise NotImplementedException("This function is not implemented by " + self.class_name)

  def __init__(self):
    '''Initializes the user interface.'''
    self.ni('__init__')

  def main_loop(self):
    '''Invokes the main control loop.'''
    self.ni('main_loop')

  def map_mode(self, map):
    '''Puts the UI in map mode.'''
    self.ni('map_mode')

  def shop_mode(self, shop):
    '''Puts the UI in shop mode.'''
    self.ni('shop_mode')

  def battle_mode(self, battle):
    '''Puts the UI in battle mode.'''
    self.ni('battle_mode')

  def conversation_mode(self, conversation):
    '''Puts the UI in conversation mode.'''
    self.ni('conversation_mode')

  def hp_tick(self, character, amount):
    '''Reports a gain in health.'''
    self.ni('hp_tick')

  def mp_tick(self, character, amount):
    '''Reports a gain in mana.'''
    self.ni('mp_tick')

  def gained_ability_level(self, character, ability):
    '''Reports an ability level gain.'''
    self.ni('gained_ability_level')

  def gained_level(self, character):
    '''Reports a level gain.'''
    self.ni('gained_level')

  def won_battle(self):
    '''Returns the UI to the map after winning a battle.'''
    self.ni('won_battle')
