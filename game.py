# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

'''Simple module for holding the game object.  Check its documentation for more information.'''

# Game is just a simple class that holds the party, ui, and anything else we
# need to be able to use from anywhere.
class Game(object):
  '''Holds the objects that need to be accessible from anywhere in the game.  Contents:
     maps: A dictionary map_name -> map
     ui: The current user interface.
     party: The hero's party.'''
  maps = {}
  ui = None
  party = None

game = Game()
