# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# WARNING: All mod formats are under heavy development.  They may change at any
# time, breaking old mods.  For this reason, we DO NOT RECOMMEND making mods
# at this time.

# This file defines the basic map tiles used in the game.  These tiles cannot
# be used directly in a map, but they are used by the standard map tiles. For
# instance, a standard "safe" tile is the combination of the "no_encounters"
# and "regen" basic tiles.

# This file has the same format as <module>/basic_tiles.py.
# For a complete guide to modding, see MODDING, in the Realism main directory.

# Like all the .py files, this is Python code.  Most mod makers should be able
# to understand and adapt it, but knowledge of Python will help, and may allow
# you to do neat tricks.

'''Basic tiles are the fundamental pieces that you put together to create normal tiles.  Each one handles one specific function, like random encounters or the regen when you take a step.'''

from engine.tile import *
from engine.shop import make_shop

class TileException(Exception):
  pass

class empty(tile):
  '''The base tile for all ordinary empty squares.'''
  type = "empty"

class no_op(empty):
  '''Dummy basic tile for anything that has no effect.'''
no_regen = no_encounters = no_op

class regen(empty):
  '''Every time the player steps on this tile, each party member gets healed slightly.'''
  def go_to(self, teleported = False):
    went = super(regen, self).go_to(teleported)
    if went:
      self.hp_mp_ticks()
    return went

class random_encounters(empty):
  '''Every time the player steps on this tile, there is a chance he may be sent into a random encounter.'''
  def go_to(self, teleported = False):
    went = super(random_encounters, self).go_to(teleported)
    if went:
      self.maybe_fight()
    return went

class door(empty):
  '''A door.  Walking into a door that's closed but unlocked will open it, but it takes a second attempt to move to this tile.'''
  save = ("open","locked")
  type = "door"
  locked = False
  open = False
  def __init__(self, map_pos, *args):
    super(door, self).__init__(map_pos)
  def go_to(self, teleported = False):
    if not self.open:
      if not self.locked:
        self.open = True
        self.type = "door_open"
        return False
    return super(door, self).go_to(teleported)

class locked_door(door):
  '''A door that starts locked.'''
  locked = True

class warp_tile(empty):
  '''A tile that attempts to warp the player to a new location on the map.'''
  def __init__(self, map_pos, target_row, target_col, *args):
    # Pass the buck.
    super(warp_tile, self).__init__(map_pos)

    # If we got ('s', 's'), the start square of the map, we preserve it.
    # Otherwise, we process row and col into integers.
    if target_row == target_col == 's':
      row = col = 's'
    else:
      row = self.argument("target row", target_row, int)
      col = self.argument("target col", target_col, int)

    # Save the target position.
    self.target_pos = (row, col)

class map_tile(warp_tile):
  '''A tile that attempts to warp the player to a new map.  If no location is specified, the player is warped to the map's start location.  Usually, only teleporters will want to omit the location.'''
  def __init__(self, map_pos, target_map, target_row = None, target_col = None, *args):
    # If we got both row and col, we just pass those into warp_tile.
    # If we got neither, we use ('s', 's'), the start square of the map.
    if target_row != None and target_col == None:
      raise MapException("target row without target col in tile map_tile on map", map_pos[0].name)
    elif target_row == None:
      (target_row, target_col) = ('s', 's')

    # Pass the buck.
    warp_tile.__init__(self, map_pos, target_row, target_col)

    # Find and store the target map.
    self.target_map = self.argument("target map", target_map, a_map)

class tutorial(empty):
  '''The first time the player steps on a tutorial tile, they are given a tutorial, which must be defined as tutorial(self) on the final tile.'''
  save = ("seen",)
  seen = False
  def __init__(self, map_pos, main_game = False, *args):
    super(tutorial, self).__init__(map_pos)
    self.main_game = self.argument("main game", main_game, bool)

  def tutorial(self):
    # TODO: Consider changing to raise.
    print TileException("No tutorial defined for %s." % self.__class__.__name__)

  def go_to(self, teleported = False):
    went = super(tutorial, self).go_to(teleported)
    if went and not self.seen:
      self.tutorial()
      self.seen = True
    return went

class shop(tile):
  '''Walking into a shop tile causes the player to enter a shop, regardless of whether he moves onto the tile.'''
  type = "shop"
  save = ("shop",)
  level = 1
  shop_args = {}

  def __init__(self, map_pos, level=None, *args):
    super(shop, self).__init__(map_pos, *args)

    if level != None:
      self.level = self.argument("shop level", level, int)

    self.shop = make_shop(self.level, **self.shop_args)

  def go_to(self, teleported = False):
    game.ui.shop_mode(self.shop)
    return super(shop, self).go_to(teleported)

class climb_tile(tile):
  '''A climb tile is simply a tile with some element of height to it.  A level 1 climb tile could be as little as a twig or bit of trash in the way.  If the party's climb level is equal to or greater than the climb tile's level, they can pass.'''
  type = "step"
  level = 1
  def __init__(self, map_pos, level = None, *args):
    super(climb_tile, self).__init__(map_pos, *args)

    if level != None:
      self.level = self.argument("climb level", level, int)

  def go_to(self, teleported = False):
    if game.party.climb_level >= self.level:
      return super(climb_tile, self).go_to(teleported)
    else:
      return False
