' This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
' and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
' Realism directory.

' WARNING: All mod formats are under heavy development.  They may change at any
' time, breaking old mods.  For this reason, we DO NOT RECOMMEND making mods
' at this time.

' This is the start map for the realism main game, which also serves as a mini-
' guide to making maps for your mods.
' For a complete guide to modding, see MODDING, in the Realism main directory.

' TODO: A "mini-guide" this ain't.  Trim it down.

' Lines starting with an apostraphe (') are comments and will be ignored when
' the map is processed.  Blank lines are also ignored.

' THE MAP
' The map file starts with an ASCII art of the map.  If some lines are shorter
' than others, the missing tiles are part of the east border.

' You can use any symbols you like except for the apostraphe (') and colon (:).
' Try not to use symbols that are easily confused (1/l, 0/O).

' Whitespace at the end of lines is trimmed.  Do not use tabs, they throw off
' the alignment of the ASCII art.

' Note that case matters.  T != t
...###...#...
.@1d2d..3d..4dt
...###...#...

' THE KEY
' The art is followed by a key.  Each symbol is given a map function.
' You can overwrite them and create your own in <module>/map_functions.py.
' If the map contains any symbols that aren't in the key, the module will output
' an error message and refuse to run.
' The symbol must be the first character on the line, followed immediately by
' a colon.  The space after is optional, but improves readability.
@: start_and_movement_tutorial

' "safe" squares have no random encounters or other special features.
' If you want ordinary terrain with random encounters, use "normal" instead.
.: safe
#: wall

' "safe_door" squares contain closed but unlocked doors.  When open, they act
' like "safe" squares.  "door" squares act like "normal" squares when open.
d: safe_door
1: object_tutorial
2: combat_tutorial

' Some functions take one or more parameters, separated by spaces.  Extra spaces
' in a row are ignored, as parameters may not be omitted.
' These two have the parameter "Main Game", which alters the tutorial dialogue
' to be in-character for the main game.  Use "False" if you want a generic
' tutorial.
' You can also just leave it off, since it defaults to False.
3: conversation_tutorial True
4: menu_tutorial True

' If no function with that name exists, the game will search for a map by that
' name, and link to any that it finds.  If no map exists, the game will output
' an error and refuse to run the module.
t: start_town 11 4

' MAP FEATURES
' After the key come a set of the features of the map as a whole.

' The four border_ features are like the key above, but apply to the borders
' of the map.
' Each one should be either wall or a map name.  You can also use a custom
' function, but returning True (move OK) will be ignored.  Choose your display
' appropriately.
border_north: wall
border_east: start_town 11 4
border_south: wall
border_west: wall

' This map has only safe spaces, so this section is for reference only.
' "normal" squares would cause encounters 20% of the time, with a safe period
' of five squares after each encounter.
encounter_frequency: 20
encounter_delay: 5

' The encounters would be against single, level 1 monsters.
' If you specify a range for either level or critters, the value will be chosen
' at random for each encounter.
encounter_min_level: 1
encounter_max_level: 1
encounter_min_critters: 1
encounter_max_critters: 1

' critter_types is a space-separated list of critter types (as defined in
' <module>/monsters.py) that can appear on this map.  Note that this only
' applies to "normal" squares.  Triggered encounters can use any critters.
critter_types: simple

' The map can either use one of the normal tilesets or define its own in 
' <module>/tilesets.py.
tileset: simple_cave
