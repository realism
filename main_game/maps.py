# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# WARNING: All mod formats are under heavy development.  They may change at any
# time, breaking old mods.  For this reason, we DO NOT RECOMMEND making mods
# at this time.

# This file defines the maps used in the game.  It has the same format as
# <module>/maps.py.
# For a complete guide to modding, see MODDING, in the Realism main directory.

# Like all the .py files, this is Python code.  Most mod makers should be able
# to understand and adapt it, but knowledge of Python will help, and may allow
# you to do neat tricks.

'''Defines the maps used in the game.'''

# This is the voodoo that processes the map files and lets them inter-link.
from engine.game_map import make_make_map
make_map = make_make_map(__file__)

# Map definitions are of the form:
# make_map("map_name")
# The actual map must be in map_name.map, in this directory.
#
# Optionally, you can give the map a name for use in your functions:
# name = make_map("map_name")

# Every mod must have a map called start.  This is where the player starts.
# TODO: REWORD, FIX
# The engine checks for the name at left first, then for a "start" passed to
# make_map.  If neither is available, the mod will not run.
start_map = make_map("start_map")

start_town = make_map("start_town")
beginner_woods = make_map("beginner_woods")
open_plains = make_map("open_plains")
