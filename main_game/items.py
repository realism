# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# WARNING: All mod formats are under heavy development.  They may change at any
# time, breaking old mods.  For this reason, we DO NOT RECOMMEND making mods
# at this time.

# This file defines the items used in the game.  It has the same format as
# <module>/items.py.
# For a complete guide to modding, see MODDING, in the Realism main directory.

# Like all the .py files, this is Python code.  Most mod makers should be able
# to understand and adapt it, but knowledge of Python will help, and may allow
# you to do neat tricks.

'''Defines the items used in the game.'''

from engine.item import *

sword = Item("sword", 100, 1)
shield = Item("shield", 75, 1)
potion = Item("potion", 50, 1)
fire_stone = Item("fire stone", 200, 1)
healing_stone = Item("healing stone", 500, 1)
