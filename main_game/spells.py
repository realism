# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# WARNING: All mod formats are under heavy development.  They may change at any
# time, breaking old mods.  For this reason, we DO NOT RECOMMEND making mods
# at this time.

# This file defines the spells used in the game.  It has the same format as
# <module>/spells.py.
# For a complete guide to modding, see MODDING, in the Realism main directory.

# Like all the .py files, this is Python code.  Most mod makers should be able
# to understand and adapt it, but knowledge of Python will help, and may allow
# you to do neat tricks.

'''Defines the spells used in the game.'''

from engine.spell import *

def heal(caster, target, battle):
  '''Ordinary life-based heal spell.'''
  mp_cost = min(caster.max_mp, int(caster.stats[will] ** 0.5))
  return plain_spell(heal, caster, target, battle, mp_cost, element=life, defensive=True)

def fireball(caster, target, battle):
  '''Ordinary fire-based damage spell.'''
  mp_cost = min(caster.max_mp, int(caster.stats[will] ** 0.5))
  return plain_spell(fireball, caster, target, battle, mp_cost, element=fire)
