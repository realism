# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# WARNING: All mod formats are under heavy development.  They may change at any
# time, breaking old mods.  For this reason, we DO NOT RECOMMEND making mods
# at this time.

# This file defines the monsters used in the game.  It has the same format as
# <module>/monsters.py.
# For a complete guide to modding, see MODDING, in the Realism main directory.

# Like all the .py files, this is Python code.  Most mod makers should be able
# to understand and adapt it, but knowledge of Python will help, and may allow
# you to do neat tricks.

'''Defines the monsters used in the game.  To remove the default monsters from the random encounter pool, write: default_monsters = []'''

#TODO: Monsters might be better implemented as classes, not objects.  I think I'm abusing monster_type's __init__ here.

from engine.monster import *
default_monsters = monsters = []

# Useful notes.
# Arguments for creating a monster:
#monster.__init__(self, being_args, ai_notify_func, ai_action_func):
#
# Elements:
#(no_element, fire, ice, lightning, metal, water, air, earth, life, death, order, chaos, void, ether, rock, paper, scissors) = range(element_count)

# Blob.  Perfectly ordinary monster, any element, no_element default.
blob = monster_type(name="blob")
monsters.append(blob)

# Bat.  High agility and accuracy, low strength and toughness.
# Any elements but Earth are allowed, Air default.
stat_adjusts = make_stat_adjusts(agility = high, accuracy = high, strength = low, toughness = low)
stat_func = adjusted_stat_func(stat_adjusts)
ok_elements = not_elements(earth)
bat = monster_type(name="bat", default_element=air, ok_elements=ok_elements, stat_func = stat_func)
monsters.append(bat)
