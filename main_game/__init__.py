# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# WARNING: All mod formats are under heavy development.  They may change at any
# time, breaking old mods.  For this reason, we DO NOT RECOMMEND making mods
# at this time.

# This file is simply a placeholder that allows the mod system to load data
# from your mod.  Python programmers may know extra tricks to use here.

'''The main_game module stores all of the code specific to Realism's gameplay.  Its contents are identical to those of a mod, but they also serve as defaults.  Mods go in <realism>/mods/my_mod_name/.  To override a default, simply redefine it in the appropriate file of your mod.'''
