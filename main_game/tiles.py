# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

# WARNING: All mod formats are under heavy development.  They may change at any
# time, breaking old mods.  For this reason, we DO NOT RECOMMEND making mods
# at this time.

# This file defines the map tiles used in the game.  It has the same format as
# <module>/tiles.py.
# For a complete guide to modding, see MODDING, in the Realism main directory.

# Like all the .py files, this is Python code.  Most mod makers should be able
# to understand and adapt it, but knowledge of Python will help, and may allow
# you to do neat tricks.

'''Holds the tile that are used on the maps.  Contrast to basic_tiles.'''

#TODO: Docstrings for each tile: useful or overkill?  I'm not sure, so I'm not
# adding them yet.

from engine.tile import *
import engine.basic_tiles as basic

start_tiles = ["start", "unsafe_start", "start_and_movement_tutorial"]

class wall(tile):
  type = "wall"
  def go_to(self, teleported = False):
    return False  # Nothing happens when you try to move through a wall.

class safe(basic.no_encounters, basic.regen):
  pass

class normal(basic.random_encounters, basic.regen):
  pass

class start(safe):
  pass

class unsafe_start(normal):
  pass

class warp_tile(basic.warp_tile, safe):
  pass

class map_tile(basic.map_tile, safe):
  pass

class tutorial(basic.tutorial, safe):
  pass

class object_tutorial(tutorial):
  # TODO: Tutorial method.
  pass

class conversation_tutorial(tutorial):
  # TODO: Tutorial method.
  pass

class combat_tutorial(tutorial):
  # TODO: Tutorial method.
  pass

class menu_tutorial(tutorial):
  # TODO: Tutorial method.
  pass

class start_and_movement_tutorial(tutorial):
  # TODO: Tutorial method.
  pass

class safe_door(basic.door, safe):
  pass

class door(basic.door, normal):
  pass

class armor_shop(basic.shop):
  type = "armor_shop"
  shop_args = {'armor': True}

class weapon_shop(basic.shop):
  type = "weapon_shop"
  shop_args = {'weapons': True}

class item_shop(basic.shop):
  type = "item_shop"
  shop_args = {'other': True}

class magic_shop(basic.shop):
  type = "magic_shop"
  shop_args = {'magic': True}

class welcome_to_start_town_ladies(wall):
  type = "lady"
  def go_to(self, teleported = False):
    game.ui.conversation_mode('The young lady smiles like a mannequin.  "Welcome to Start Town!"')

class save_point(wall):
  type = "save_point"
  def go_to(self, teleported = False):
    #TODO
    pass

class climb_tile(basic.climb_tile, normal):
  pass

class twig(climb_tile):
  type = "twig"
  level = 1

class branch(climb_tile):
  type = "branch"
  level = 2

class gate_to_open_plains(map_tile):
  type = "gate"
  save = ("open",)
  def __init__(self, map_pos, *args):
    super(gate_to_open_plains, self).__init__(map_pos, "open_plains")
    self.open = False
  def go_to(self, teleported = False):
    #TODO: Let the gate be opened.
    if self.open:
      return map_tile.go_to(self)
    else:
      return False

class beginner_woods_boss(wall):
  type = "evil_tree"
  #TODO: Make a boss.
