# This file is part of Realism, which is Copyright FunnyMan3595 (Charlie Nolan)
# and licensed under the GNU GPL v3.  For more details, see LICENSE in the main
# Realism directory.

'''Handles argument parsing.  Provides the following values:
mod: The name of the mod that has been requested.'''

from sys import argv

if len(argv) > 1:
  mod = argv[-1]
else:
  mod = ""
